package com.pccpz.exception;

public class MyException extends RuntimeException{
    public MyException() {
        super("予期せぬエラーが発生しました。");
    }
}
