package com.pccpz.controller;

import com.pccpz.entities.Employee;
import com.pccpz.entities.User;
import com.pccpz.mapper.EmployeeMapper;
import com.pccpz.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

@Controller
public class LoginController {

    @Autowired
    UserMapper userMapper;
    @Autowired
    EmployeeMapper employeeMapper;

    @PostMapping(value = "/user/login")
    public String login(User user, Map<String, Object> map, HttpSession session){
        List<User> userList = userMapper.getUserList(user);
        if(userList.size()==1){
            user = userList.get(0);
            Employee employee =employeeMapper.getEmployeeById(user.getEmployeeId());
            session.setAttribute("loginUser", user);
            session.setAttribute("loginEmployee", employee);
            //登录成功后，为防止表单重复提交，可以重定向到主页面
            return "redirect:/main.html";
        }else{
            map.put("msg", "用户名密码错误");
            return "login";
        }
    }
}
