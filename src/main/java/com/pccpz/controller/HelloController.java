package com.pccpz.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

@Controller
public class HelloController {

    @RequestMapping("/success")
    public String hello(Map<String,Object> map){
        map.put("hello1","こんにちは");
        map.put("hello2","你好");
        return "success";
    }
}
