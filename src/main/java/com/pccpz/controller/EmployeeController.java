package com.pccpz.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pccpz.entities.Department;
import com.pccpz.entities.Employee;
import com.pccpz.exception.MyException;
import com.pccpz.service.DepartmentService;
import com.pccpz.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


import java.util.List;

@Controller
public class EmployeeController {

    private static final int PAGE_SIZE = 2;
    @Autowired
    EmployeeService employeeService;
    @Autowired
    DepartmentService departmentService;

    //员工列表
    @GetMapping("/emps")
    public String employeeList(@RequestParam (value="pageNum",defaultValue="1")Integer pageNum,Model model){
        PageHelper.startPage(pageNum,PAGE_SIZE);
        List<Employee> employeeList = employeeService.getEmployeeList();
        PageInfo<Employee> pageInfo = new PageInfo<>(employeeList,3);
        model.addAttribute("pageInfo", pageInfo);
        return "emp/list";
    }

    //员工删除
    @DeleteMapping("/emp/{id}")
    public String deleteEmployee(@PathVariable("id") Integer id) {
        if(id ==2) {
            throw new MyException();
        }
        int cnt = employeeService.deleteEmployee(id);
        return "redirect:/emps";
    }
    /**********************************************************************************************/
    //员工添加
    @GetMapping("/emp")
    public String toAddPage(Model model) {
        List<Department> departments =  departmentService.getDepartmentList();
        model.addAttribute("departments", departments);
        return "emp/add";
    }
    //添加员工实际逻辑
    @PostMapping(value = "/emp")
    public String addEmployee(Employee employee) {
        int cnt = employeeService.insertEmp(employee);
        return "redirect:/emps";
    }
    /**********************************************************************************************/
    //来到修改页面，查出当前员工，在页面回显
    @GetMapping("/emp/{id}")
    public String toEditPage(@PathVariable("id") Integer id, Model model) {
        List<Department> departments =  departmentService.getDepartmentList();
        model.addAttribute("departments", departments);
        Employee employee = employeeService.getEmployeeById(id);
        model.addAttribute("emp", employee);
        return "emp/add";
    }

    //员工修改；需要提交员工id；
    @PutMapping("/emp")
    public String updateEmployee(Employee employee) {
        int cnt = employeeService.updateEmployee(employee);
        return "redirect:/emps";
    }

}
