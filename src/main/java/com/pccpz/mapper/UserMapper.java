package com.pccpz.mapper;

import com.pccpz.entities.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserMapper {

    public List<User> getUserList(User user);

}
