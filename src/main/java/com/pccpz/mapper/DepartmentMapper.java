package com.pccpz.mapper;

import com.pccpz.entities.Department;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface DepartmentMapper {

    public List<Department> getDepartmentList();

    public Department getDepartmentById(Integer id);

    public int insertDepartment(Department department);
}
