package com.pccpz.mapper;

import com.pccpz.entities.Employee;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface EmployeeMapper {

    public List<Employee> getEmployeeList();

    public Employee getEmployeeById(Integer id);

    public int insertEmployee(Employee employee);

    public int deleteEmployee(Integer id);

    public int updateEmployee(Employee employee);
}
