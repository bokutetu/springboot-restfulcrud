package com.pccpz.service;

import com.pccpz.entities.Department;
import com.pccpz.mapper.DepartmentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Service
public class DepartmentService {

    @Autowired
    DepartmentMapper departmentMapper;

    public List<Department> getDepartmentList(){

        return departmentMapper.getDepartmentList();
    }

    public Department getDepartmentById(Integer id){
        return departmentMapper.getDepartmentById(id);
    }
}
