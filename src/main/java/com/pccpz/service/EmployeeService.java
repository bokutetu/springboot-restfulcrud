package com.pccpz.service;

import com.pccpz.entities.Employee;
import com.pccpz.mapper.EmployeeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

    @Autowired
    EmployeeMapper employeeMapper;

    public List<Employee> getEmployeeList() {
        return employeeMapper.getEmployeeList();
    }

    public Employee getEmployeeById(Integer id) {
        return employeeMapper.getEmployeeById(id);
    }

    public int insertEmp(Employee employee) {

        return employeeMapper.insertEmployee(employee);
    }

    public int deleteEmployee(Integer id) {
        return employeeMapper.deleteEmployee(id);
    }

    public int updateEmployee(Employee employee) {
        return employeeMapper.updateEmployee(employee);
    }
}
